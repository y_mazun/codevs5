all: codevs2015

test: codevs2015
	./codevs2015 < sample

debug: codevs2015
	gdb codevs2015 core

release: main.cpp
	g++ -Wextra -std=gnu++0x -o codevs2015 -O3 -pthread main.cpp

codevs2015: main.cpp
	g++ -DDEBUG -Wextra -std=gnu++0x -o codevs2015 -pthread -O2 -g main.cpp

clean:
	rm -f codevs2015

