package hiretsu;

import contest.game.ai.*;
import contest.game.ai.xx.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class Hiretsu {
    public static void main(String[] args) {
	AI ai = new HiretsuAI();
	BufferedReader stdReader =
	    new BufferedReader(new InputStreamReader(System.in));

	String acc = "";
	String line;

	try {
	    while((line = stdReader.readLine()) != null) {
		acc += line + "\n";
	    }
	} catch (Exception e) {
	}

	System.out.println(ai.think(acc));
    }
}
