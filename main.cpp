#include <algorithm>
#include <queue>
#include <vector>
#include <array>
#include <tuple>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <numeric>
#include <thread>
#include <sstream>
#include <map>

#ifndef LOCAL
#define NDEBUG
#endif

#include <cassert>

#ifdef __linux
#include <unistd.h>
#elif __MINGW32__
#include <windows.h>
#include <winbase.h>
#include "thread.h"
#endif

#ifndef CORE_COUNT
int getCpuNum()
{
#ifdef __linux
  // for linux
  return sysconf(_SC_NPROCESSORS_CONF);
#elif __MINGW32__
  // for windows and wine
  SYSTEM_INFO info;
  GetSystemInfo(&info);

  return info.dwNumberOfProcessors;
#endif
}
#endif


namespace CodeVS {
  enum Dir {
    Up    = 0,
    Left  = 1,
    Right = 2,
    Down  = 3,

    None  = 4,
  };

  constexpr int dx[] = {0, -1, 1, 0};
  constexpr int dy[] = {-1, 0, 0, 1};

  static const char * const dirStr = "ULRD";

  inline Dir reverse(Dir dir) {
    return (Dir)((~(int)dir) & ((1 << 2) - 1));
  }

  inline char dir2c(Dir dir) {
    return dirStr[(int)dir];
  }

  enum ObjectType {
    T_Ninja = 0,
    T_Soul  = 1,
    T_Dog   = 2,
    T_Stone = 3,

    T_Wall  = 4,
  };

  constexpr bool canOverlap[4][4] = {
    {1, 1, 1, 0},
    {1, 0, 1, 0},
    {1, 1, 0, 1},
    {0, 0, 1, 0}
  };

  struct Ninja {
    static constexpr int tag = ObjectType::T_Ninja;

    int id;
    int x;
    int y;

    Ninja() : id(0), x(0), y(0) {}
    Ninja(int id, int x, int y) : id(id), x(x), y(y) {}

    void debug() const {
      fprintf(stderr, "Ninja: %d %d %d\n", id, x, y);
    }
  };

  struct Dog {
    static constexpr int tag = ObjectType::T_Dog;

    int id;
    int x;
    int y;

    Dog() : id(0), x(0), y(0) {}
    Dog(int id, int x, int y) : id(id), x(x), y(y) {}

    void debug() const {
      fprintf(stderr, "Dog: %d %d %d\n", id, x, y);
    }
  };

  struct Soul {
    static constexpr int tag = ObjectType::T_Dog;

    int x;
    int y;

    Soul() : x(0), y(0) {}
    Soul(int x, int y) : x(x), y(y) {}

    void debug() const {
      fprintf(stderr, "Soul: %d %d\n", x, y);
    }
  };

  struct Field {
    static constexpr int width = 14;
    static constexpr int height = 17;

    char field[height][width];
    std::array<Ninja, 2> ninja;
    int soulCount;
    std::vector<Dog> dog;
    std::vector<Soul> soul;

    Field() : soul(0) {
      memset(field, sizeof(field), 0);
    }

    void debug() const {
      fprintf(stderr, "Field:\n");
      for(int i = 0; i < height; i++){
	for(int j = 0; j < width; j++)
	  fprintf(stderr, "%c", field[i][j]);
	fprintf(stderr, "\n");
      }
      ninja[0].debug();
      ninja[1].debug();
      for(const auto &d : dog){
	d.debug();
      }
    }
  };

  struct GameInfo {
    int turn;
    int restMS;
    std::array<int, 8> cost;

    Field myField;
    Field enemyField;

    GameInfo(){}

    GameInfo(int turn, int restMS, std::array<int, 8> cost, Field &&myField, Field &&enemyField)
      : turn(turn), restMS(restMS), cost(cost), myField(myField), enemyField(enemyField) {}

    void debug() const {
      fprintf(stderr, "restMS: %d\n", restMS);
      for(int c : cost) fprintf(stderr, "%d ", c); fprintf(stderr, "\n");

      fprintf(stderr, "myField\n"); myField.debug();
      fprintf(stderr, "enemyField\n"); enemyField.debug();
    }
  };

  enum SkillType {
    FastMove             = 0,
    DropStoneForSelf     = 1,
    DropStoneForEnemy    = 2,
    DestroyStoneForSelf  = 3,
    DestroyStoneForEnemy = 4,
    AvatarForSelf        = 5,
    AvatarForEnemy       = 6,
    TransportDogs        = 7,
  };

  int skillArgCounts[] = {
    0, 2, 2, 2, 2, 2, 2, 1
  };

  struct SkillInfo {
    SkillType type;
    std::vector<int> args;

    SkillInfo(SkillType type) : type(type) {}
    SkillInfo(SkillType type, int arg1) : type(type), args({arg1}) {}
    SkillInfo(SkillType type, int arg1, int arg2) : type(type), args({arg1, arg2}) {}
  };

  bool operator < (const SkillInfo &lhs, const SkillInfo &rhs) {
    if(lhs.type != rhs.type) return lhs.type < rhs.type;
    return lhs.args < rhs.args;
  }

  SkillInfo fastMove() {
    return SkillInfo(FastMove);
  }

  SkillInfo dropStoneForSelf(int x, int y) {
    return SkillInfo(DropStoneForSelf, y, x);
  }

  SkillInfo dropStoneForEnemy(int x, int y) {
    return SkillInfo(DropStoneForEnemy, y, x);
  }

  SkillInfo destroyStoneForSelf(int x, int y) {
    return SkillInfo(DestroyStoneForSelf, y, x);
  }

  SkillInfo destroyStoneForEnemy(int x, int y) {
    return SkillInfo(DestroyStoneForEnemy, y, x);
  }

  SkillInfo avatarForSelf(int x, int y) {
    return SkillInfo(AvatarForSelf, y, x);
  }

  SkillInfo avatarForEnemy(int x, int y) {
    return SkillInfo(AvatarForEnemy, y, x);
  }

  SkillInfo transportDogs(int id) {
    return SkillInfo(TransportDogs, id);
  }

  struct Input {
    char buff[1024 * 24];
  private:
    int turn;
    char *ptr;

    void skipToken(int count = 1) {
      char c[32];
      for(int i = 0; i < count; i++) {
	scanf("%s", c);
	ptr += sprintf(ptr, "%s", c);
	if(i != count - 1) ptr += sprintf(ptr, " ");
      }
    }

    void newLine() {
      ptr += sprintf(ptr, "\n");
    }

    Field readField() {
      Field ret;

      scanf("%d", &ret.soulCount);
      ptr += sprintf(ptr, "%d\n", ret.soulCount);

      skipToken(2);
      newLine();

      for(int i = 0; i < Field::height; i++) {
	char buff[Field::width + 1];
	scanf("%s", buff);
	memcpy(ret.field[i], buff, Field::width);
	ptr += sprintf(ptr, "%s\n", buff);
      }

      skipToken(1); newLine();
      for(int i = 0; i < 2; i++) {
	Ninja ninja;
	scanf("%d%d%d", &ninja.id, &ninja.y, &ninja.x);
	ret.ninja[i] = ninja;
	ptr += sprintf(ptr, "%d %d %d\n", ninja.id, ninja.y, ninja.x);
      }

      int dogCount;
      scanf("%d", &dogCount);
      ptr += sprintf(ptr, "%d\n", dogCount);
      for(int i = 0; i < dogCount; i++) {
	int id, x, y;
	scanf("%d%d%d", &id, &y, &x);
	ret.dog.emplace_back(id, x, y);
	ptr += sprintf(ptr, "%d %d %d\n", id, y, x);
      }

      int soulCount;
      scanf("%d", &soulCount);
      ptr += sprintf(ptr, "%d\n", soulCount);
      for(int i = 0; i < soulCount; i++) {
	int x, y;
	scanf("%d%d", &y, &x);
	ret.soul.emplace_back(x, y);
	ptr += sprintf(ptr, "%d %d\n", y, x);
      }

      skipToken(8);
      newLine();

      return ret;
    }

  public:
    Input() : turn(0) {
    }

    std::string asStr() const {
      puts(buff);
      return buff;
    }

    GameInfo read() {
      int restMS;
      std::array<int, 8> cost;
      ptr = buff; buff[0] = '\0';

      if(scanf("%d", &restMS) == EOF) {
	throw 0;
      }
      ptr += sprintf(ptr, "%d\n", restMS);

      skipToken(); newLine();
      for(int i = 0; i < 8; i++) {
	scanf("%d", &cost[i]);
	ptr += sprintf(ptr, "%d%c", cost[i], i == 7 ? '\n' : ' ');
      }

      Field myField = readField();
      Field enemyField = readField();

      return GameInfo(turn++, restMS, cost, std::move(myField), std::move(enemyField));
    }
  };

  struct Output {
    std::vector<Dir> move[2];
    std::vector<SkillInfo> skill;

    Output() {}

    void moveNinja(const Ninja &ninja, Dir dir1) {
      const int id = ninja.id;
      move[id] = { dir1 };
    }

    void moveNinja(const Ninja &ninja, Dir dir1, Dir dir2) {
      const int id = ninja.id;
      move[id] = { dir1, dir2 };
    }

    void moveNinja(const Ninja &ninja, const std::vector<Dir> &dir) {
      const int id = ninja.id;
      move[id] = dir;
    }

    void doSkill(const SkillInfo &info) {
      skill = { info };
    }

    void write() const {
      printf("%d\n", 2 + (int)skill.size());

      for(const auto s : skill) {
	printf("%d", (int)s.type);

	for(const auto arg : s.args) {
	  printf(" %d", arg);
	}

	puts("");
      }

      for(const auto m : move) {
	for(const auto d : m) {
	  putchar(dir2c(d));
	}
	puts("");
      }

      fflush(stdout);
    }
  };
}

namespace CodeVS { namespace AI {
#ifdef CORE_COUNT
    constexpr int coreCount = CORE_COUNT;
#else
    const int coreCount = getCpuNum();
#endif

    // Evaluation Parameters
    static double rSoulDogDist = 100;
    static double rSoulDist  = 10;
    static double rDogDist   = 20;
    static double rDogRatio  = 0;
    static double rNinDist   = 0;
    static double rMovable   = 2000;
    static double rSoulCount = 300;
    static double rSoulGet   = 500;
    static double rStoneDestroy = 300;
    static double rDogCount  = -1000;
    static double rFastMove  = 800;

    void readConfig(const char *configFile) {
      char buff[256];
      double val;

      FILE *fp = fopen(configFile, "r");
      if(fp == NULL) {
	fprintf(stderr, "There is no config.\n");
	fflush(stderr);
	return;
      }

      while(fscanf(fp, "%s %lf", buff, &val) != EOF) {
#define M(s) else if(strncmp(buff, #s, strlen(buff)) == 0) { s = val; }
	if(false){}
	M(rSoulDogDist)
	M(rSoulDist)
	M(rDogDist)
	M(rDogRatio)
	M(rNinDist)
	M(rMovable)
	M(rSoulCount)
	M(rSoulGet)
	M(rStoneDestroy)
	M(rDogCount)
	else {
	  fprintf(stderr, "Unknown key: %s\n", buff);
	  fflush(stderr);
	}
#undef M
      }
      fprintf(stderr, "Read config done.\n");
      fflush(stderr);

      fclose(fp);
    }

    const int dx2[] = {
      0, -1, 0, 1, -2, -1, 0, 1, 2, -1, 0, 1, 0
    };
    const int dy2[] = {
      -2, -1, -1, -1, 0, 0, 0, 0, 0, 1, 1, 1, 2
    };
    const int size2 = sizeof(dx2) / sizeof(int);

    enum Object {
      None    = 0,
      Wall    = 1 << 0,
      Stone   = 1 << 1,

      Ninja1 = 1 << 2,
      Ninja2 = 1 << 3,

      NinDog  = 1 << 4,

      NinSoul = 1 << 12,

      End = 1 << 20,
      DogMask  = (NinSoul - 1) ^ (NinDog - 1),
      SoulMask = (End - 1) ^ (NinSoul - 1)
    };

    inline constexpr bool isWall(int obj){ return (obj & Wall) != 0; }
    inline constexpr bool hasStone(int obj){ return (obj & Stone) != 0; }
    inline constexpr bool canEnter(int obj){ return (!hasStone(obj)) && (!isWall(obj)); }
    inline constexpr bool hasNinja(int obj){ return (obj & (Ninja1 | Ninja2)) != 0; }
    inline constexpr bool hasNinDog(int obj){ return (obj & DogMask) != 0; }
    inline constexpr int ninDogID(int obj){ return ((obj & DogMask) >> 4) - 1; }
    inline constexpr bool hasNinSoul(int obj){ return (obj & SoulMask) != 0; }
    inline constexpr int ninSoulID(int obj){ return ((obj & SoulMask) >> 12) - 1; }


    inline constexpr int ninjaOf(int ninjaID){ return ninjaID == 0 ? Ninja1 : Ninja2; }

    constexpr int width = CodeVS::Field::width;
    constexpr int height = CodeVS::Field::height;

    typedef std::array<std::array<int, width>, height> EncodedField;

    struct Position {
      int x;
      int y;

      Position(int x, int y) : x(x), y(y) {}

      template<typename T>
      Position(const T &t) : x(t.x), y(t.y) {}

      Position move(Dir dir) const {
	return Position(x + dx[dir], y + dy[dir]);
      }

      bool isValid() const {
	return 0 <= x && x < width && 0 <= y && y < height;
      }
    };

    bool operator == (const Position &lhs, const Position &rhs) {
      return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    constexpr int inf = 1e8;
    typedef std::array<std::array<int, width>, height> Distances;

    template<typename CanGo>
    Distances bfs(const std::vector<Position> &starts, CanGo canGo);

    template<typename CanGo, typename F>
    Distances bfs(const std::vector<Position> &starts, CanGo canGo, F f);

    struct AnalyzedField {
    public:
      EncodedField field;
      const GameInfo &game;

      std::vector<Position> ninjas;
      std::vector<Position> dogs;
      std::vector<Position> souls;

      std::vector<bool> soulsGot;
      std::vector<bool> dogsTransported;

      Distances estimatedDogDistance;    // ** not updated while simulation **
      std::vector<Position> farFromDogs; // ** not updated while simulation **

      Distances estimatedNinjaDistance;    // ** not updated while simulation **
      std::vector<Position> farFromNinjas; // ** not updated while simulation **

      Distances estimatedSoulDistance;   // ** not updated while simulation **
      std::vector<Position> farFromSouls; // ** not updated while simulation **

      int soulCount;
      int turn;
      int movedStone;
      int dogCount;
      int destroyedStone;

      double storedScore;

      AnalyzedField(const GameInfo &game) : game(game), soulCount(0), turn(0), movedStone(0), dogCount(0), storedScore(0.0) {}

      std::vector<Position> soulsNotGot() const {
	std::vector<Position> ret; ret.reserve(souls.size());
	for(int i = 0; i < (int)souls.size(); i++)
	  if(!soulsGot[i]) ret.push_back(souls[i]);
	return ret;
      }

      typedef std::function<void (AnalyzedField&)> Undo;

      int get(Position position) const {
	return field[position.y][position.x];
      }

      int &get(Position position) {
	return field[position.y][position.x];
      }

      int operator[](Position position) const {
	return get(position);
      }

      int &operator[](Position position) {
	return get(position);
      }

      bool canSpell(SkillType type) const {
	return game.cost[(int)type] <= soulCount;
      }

      bool canMoveNinja(Position from, Dir dir) const {
	const Position to = from.move(dir);
	const int toObj = get(to);
	if(canEnter(toObj)) return true;

	if(hasStone(toObj)){
	  const Position to2 = to.move(dir);
	  const int to2Obj = get(to2);
	  return canEnter(to2Obj) && !hasNinDog(to2Obj) && !hasNinja(to2Obj);
	}

	return false;
      }

      void moveBit(Position a, Position b, int bit) {
	assert((get(a) & bit) != 0);
	assert((get(b) & bit) == 0);
	get(a) ^= bit;
	get(b) ^= bit;
	assert((get(a) & bit) == 0);
	assert((get(b) & bit) != 0);
      }

      void moveBits(Position a, Position b, int mask) {
	const int aa = get(a);
	const int bb = get(b);
	assert((aa & mask) != 0);
	assert((bb & mask) == 0);
	const int move = mask & get(a);
	get(a) ^= move;
	get(b) ^= move;
	assert((get(b) & mask) != 0);
	assert((get(a) & mask) == 0);
      }

      void clearBits(Position a, int mask) {
	const int aa = get(a);
	assert((aa & mask) != 0);
	const int move = mask & get(a);
	get(a) ^= move;
	assert((get(a) & mask) == 0);
      }

      void setBits(Position a, int bits) {
	get(a) ^= bits;
      }

      bool isGameOver(int id) const {
	return hasNinDog(get(ninjas[id]));
      }

      bool isGameOver() const {
	return isGameOver(0) || isGameOver(1);
      }

      Undo moveNinja(int ninjaID, Dir dir) {
	assert((get(ninjas[ninjaID]) & ninjaOf(ninjaID)) != 0); // Validate ninja position
	assert(canMoveNinja(ninjas[ninjaID], dir)); // Validate if ninja can move

	const Position orig = ninjas[ninjaID];
	const Position next = ninjas[ninjaID].move(dir);

	moveBit(ninjas[ninjaID], next, ninjaOf(ninjaID));
	ninjas[ninjaID] = next;

	const bool moveStone = hasStone(get(next));
	const Position nextStone = next.move(dir);
	if(moveStone) {
	  movedStone++;
	  moveBit(next, nextStone, Stone);
	}

	const bool getSoul = hasNinSoul(get(next));
	const int soulID = getSoul ? ninSoulID(get(next)) : -1;
	if(getSoul) {
	  soulCount += 2;
	  soulsGot[soulID] = true;
	  clearBits(next, SoulMask);
	}

	assertField();

	return [=](AnalyzedField &field) {
	  field.ninjas[ninjaID] = orig;
	  field.moveBit(next, orig, ninjaOf(ninjaID));

	  if(moveStone) {
	    field.movedStone--;
	    field.moveBit(nextStone, next, Stone);
	  }

	  if(getSoul) {
	    field.soulCount -= 2;
	    field.setBits(next, (soulID + 1) << 12);
	    field.soulsGot[soulID] = false;
	  }
	};
      }

      bool canMoveNinja(int ninjaID, std::vector<Dir> dirs) {
	std::vector<Undo> undos;
	bool ret = true;

	assertField();
	for(int i = 0; i < (int)dirs.size(); i++) {
	  if(canMoveNinja(ninjas[ninjaID], dirs[i])) {
	    if(i != (int)dirs.size() - 1) {
	      undos.emplace_back(moveNinja(ninjaID, dirs[i]));
	    }
	    assertField();
	  }else{
	    ret = false;
	    assertField();
	    break;
	  }
	}

	assertField();

	for(auto it = undos.rbegin(); it != undos.rend(); ++it) {
	  (*it)(*this);
	}

	assertField();

	return ret;
      }

      Undo moveDog1Step(const std::vector<Position> &avatars = std::vector<Position>()) {
	std::vector<Position> av;

	for(const auto avatar : avatars) {
	  if(canEnter(get(avatar)))
	    av.push_back(avatar);
	}

	const std::vector<Position> &src = (int)avatars.size() == 0 ? ninjas : av;
	const auto distances = bfs(src, [&](Position position){ return canEnter(get(position)); });

	std::vector<Dir> moves(dogs.size(), Dir::None);
	std::vector<std::pair<int, int> > v; v.reserve(dogCount);

	for(int i = 0; i < (int)dogs.size(); i++) {
	  if(dogsTransported[i]) continue;
	  const int x = dogs[i].x;
	  const int y = dogs[i].y;
	  const int d = distances[y][x];
	  v.emplace_back(d, i);
	}

	std::sort(v.begin(), v.end());

	for(const auto a : v) {
	  const int i = a.second;
	  const int d = a.first;
	  const auto cur = dogs[i];

	  for(int dir = 0; dir < 4; dir++) {
	    const auto next = dogs[i].move((Dir)dir);
	    const int x = next.x;
	    const int y = next.y;
	    if(distances[y][x] + 1 == d && !hasNinDog(get(next))) {
	      moves[i] = (Dir)dir;
	      dogs[i] = next;
	      moveBits(cur, next, DogMask);
	      break;
	    }
	  }
	}

	return [=](AnalyzedField &field) {
	  for(auto it = v.rbegin(); it != v.rend(); ++it) {
	    const int i = it->second;
	    const auto dir = moves[i];
	    if(dir == Dir::None) continue;

	    const auto cur  = field.dogs[i];
	    const auto rev  = reverse(dir);
	    const auto prev = cur.move(rev);

	    field.dogs[i] = prev;
	    field.moveBits(cur, prev, DogMask);
	  }
	};
      }

      void assertField() const {
#ifdef NDEBUG
	return;
#endif
	for(int i = 0; i < 2; i++) {
	  const auto ninja = ninjas[i];
	  assert(hasNinja(get(ninja)) && (get(ninja) & ninjaOf(i)) != 0);
	}

	for(int i = 0; i < (int)dogs.size(); i++) {
	  const auto dog = dogs[i];
	  if(dogsTransported[i]){
	    assert(!hasNinDog(get(dog)) || ninDogID(get(dog)) != i);
	  }else{
	    assert(hasNinDog(get(dog)) && ninDogID(get(dog)) == i);
	  }
	}

	for(int i = 0; i < (int)souls.size(); i++) {
	  const auto soul = souls[i];
	  if(soulsGot[i]) {
	    assert(!hasNinSoul(get(soul)));
	  } else {
	    assert(hasNinSoul(get(soul)));
	    assert(ninSoulID(get(soul)) == i);
	  }
	}

	for(int i = 0; i < height; i++) {
	  for(int j = 0; j < width; j++) {
	    const int v = field[i][j];
	    if(hasStone(v) || isWall(v)) {
	      assert(!hasNinja(v) && !hasNinDog(v));
	    }
	  }
	}
      }

      Undo addStone(int x, int y) {
	assert(!hasStone(get(Position(x, y))));
	get(Position(x, y)) ^= Object::Stone;

	return [=](AnalyzedField &field) {
	  assert(hasStone(get(Position(x, y))));
	  field.get(Position(x, y)) ^= Object::Stone;
	};
      }

      Undo simulateDropStone(int x, int y) {
	assert(!hasStone(get(Position(x, y))));
	const int cost = game.cost[SkillType::DropStoneForSelf];
	soulCount -= cost;
	get(Position(x, y)) ^= Object::Stone;

	return [=](AnalyzedField &field) {
	  assert(hasStone(get(Position(x, y))));
	  field.soulCount += cost;
	  field.get(Position(x, y)) ^= Object::Stone;
	};
      }

      Undo simulateDestroyStone(int x, int y) {
	assert(hasStone(get(Position(x, y))));
	const int cost = game.cost[SkillType::DestroyStoneForSelf];
	soulCount -= cost;
	destroyedStone++;
	get(Position(x, y)) ^= Object::Stone;

	return [=](AnalyzedField &field) {
	  assert(!hasStone(get(Position(x, y))));
	  field.soulCount += cost;
	  field.destroyedStone--;
	  field.get(Position(x, y)) ^= Object::Stone;
	};
      }

      Undo simulateTransportDogs(int id) {
	const auto ninja = ninjas[id];
	const int cost = game.cost[SkillType::TransportDogs];
	soulCount -= cost;

	std::vector<int> ids;
	const int org = dogCount;
	for(int y = -1; y <= 1; y++){
	  for(int x = -1; x <= 1; x++){
	    const Position p(ninja.x + x, ninja.y + y);
	    if(hasNinDog(get(p))) {
	      const int id = ninDogID(get(p));
	      ids.push_back(id);
	      dogCount--;
	      dogsTransported[id] = true;
	      clearBits(p, DogMask);
	    }
	  }
	}

	return [=](AnalyzedField &field) {
	  field.soulCount += cost;
	  field.dogCount = org;
	  for(int id : ids) {
	    const auto dog = field.dogs[id];
	    field.dogsTransported[id] = false;
	    field.field[dog.y][dog.x] |= (id + 1) << 4;
	  }
	};
      }

      Undo simulateSkill(const SkillInfo &skill) {
	switch(skill.type) {
	case SkillType::DropStoneForSelf:
	  return simulateDropStone(skill.args[1], skill.args[0]);
	case SkillType::DestroyStoneForSelf:
	  return simulateDestroyStone(skill.args[1], skill.args[0]);
	case SkillType::TransportDogs:
	  return simulateTransportDogs(skill.args[0]);
	default:
	  const int cost = game.cost[skill.type];
	  soulCount -= cost;
	  return [=](AnalyzedField &field) {
	    field.soulCount += cost;
	  };
	}
      }

      Undo simulateTurn(const Output &myTactics) {
	std::vector<Undo> undos;
	std::vector<Position> avatars;

	for(const auto skill : myTactics.skill) {
	  undos.emplace_back(simulateSkill(skill));

	  // special case
	  if(skill.type == AvatarForSelf) {
	    avatars.emplace_back(skill.args[1], skill.args[0]);
	  }
	}

	for(int i = 0; i < 2; i++) {
	  const auto &move = myTactics.move[i];
	  for(const auto dir : move) {
	    if(canMoveNinja(i, std::vector<Dir>({dir}))) {
	      undos.emplace_back(moveNinja(i, dir));
	    }
	  }
	}

	undos.emplace_back(moveDog1Step(avatars));

	assertField();
	return [=](AnalyzedField &field) {
	  field.applyUndos(undos);
	};
      }

      Undo addScore(double score) {
	storedScore += score;
	return [=](AnalyzedField &field) {
	  field.storedScore -= score;
	};
      }

      // If ninja die when stone drops properly
      bool isDangerousMove(int ninjaID, Dir dir) const {
	Position pos = ninjas[ninjaID];
	bool danger = false;

	pos = pos.move(dir); danger |= (hasStone(get(pos)) || isWall(get(pos)));
	pos = pos.move(dir); danger |= (hasStone(get(pos)) || isWall(get(pos)));

	if(danger) {
	  const Position ninja = ninjas[ninjaID];
	  for(int i = 0; i < 4; i++) {
	    const Dir d = (Dir)i;
	    const Position pos = ninja.move(d);
	    if(hasNinDog(get(pos))) {
	      return true;
	    }
	  }
	}
	return false;
      }

      template<typename F>
      void trySimulate(const Output &output, F f) {
	auto undo = simulateTurn(output);
	f(*this);
	undo(*this);
      }

      void debug() const {
	for(int i = 0; i < height; i++) {
	  for(int j = 0; j < width; j++) {
	    const int v = field[i][j];

	    char c = ' ';
	    if(isWall(v)) {
	      c = '#';
	    } else if(hasNinja(v)) {
	      c = 'N';
	    } else if(hasNinDog(v)) {
	      c = 'D';
	    } else if(hasNinSoul(v)) {
	      c = 'S';
	    } else if(hasStone(v)) {
	      c = 'X';
	    }

	    fprintf(stderr, "%c", c);
	  }

	  fprintf(stderr, "\n");
	}
      }

      void applyUndos(const std::vector<Undo> &undos) {
	for(auto it = undos.rbegin(); it != undos.rend(); ++it)
	  (*it)(*this);
      }
    };

    template<typename CanGo>
    Distances bfs(const std::vector<Position> &starts, CanGo canGo){
      Distances ret;

      for(int i = 0; i < (int)ret.size(); i++) {
	for(int j = 0; j < (int)ret[i].size(); j++) {
	  ret[i][j] = inf;
	}
      }

      using std::make_pair;
      std::queue<Position> q;

      for(const auto p : starts) {
	ret[p.y][p.x] = 0;
	q.push(p);
      }

      while(!q.empty()) {
	const auto d = q.front(); q.pop();
	const int x = d.x;
	const int y = d.y;

	for(int i = 0; i < 4; i++) {
	  const auto next = d.move((Dir)i);
	  const int xx = next.x;
	  const int yy = next.y;

	  if(ret[yy][xx] == inf && canGo(next)) {
	    q.push(next);
	    ret[yy][xx] = ret[y][x] + 1;
	  }
	}
      }

      return ret;
    }

    template<typename CanGo, typename F>
    Distances bfs(const std::vector<Position> &starts, CanGo canGo, F f){
      Distances ret;

      for(int i = 0; i < (int)ret.size(); i++) {
	for(int j = 0; j < (int)ret[i].size(); j++) {
	  ret[i][j] = inf;
	}
      }

      using std::make_pair;
      std::queue<Position> q;

      for(const auto p : starts) {
	ret[p.y][p.x] = 0;
	f(ret[p.y][p.x], p.x, p.y);
	q.push(p);
      }

      while(!q.empty()) {
	const auto d = q.front(); q.pop();
	const int x = d.x;
	const int y = d.y;

	for(int i = 0; i < 4; i++) {
	  const auto next = d.move((Dir)i);
	  const int xx = next.x;
	  const int yy = next.y;

	  if(ret[yy][xx] == inf && canGo(next)) {
	    q.push(next);
	    ret[yy][xx] = ret[y][x] + 1;
	    f(ret[yy][xx], xx, yy);
	  }
	}
      }

      return ret;
    }

    AnalyzedField analyze(const CodeVS::Field &field, const CodeVS::GameInfo &game) {
      AnalyzedField ret(game);

      for(int i = 0; i < height; i++) {
	for(int j = 0; j < width; j++) {
	  const char c = field.field[i][j];
	  ret.field[i][j] = None;
	  if(c == 'W') {
	    ret.field[i][j] |= Wall;
	  } else if(c == 'O') {
	    ret.field[i][j] |= Stone;
	  }
	}
      }

      ret.ninjas = std::vector<Position>(field.ninja.begin(), field.ninja.end());
      ret.dogs   = std::vector<Position>(field.dog.begin(), field.dog.end());
      ret.dogsTransported = std::vector<bool>(field.dog.size(), false);
      ret.dogCount = ret.dogs.size();
      ret.souls  = std::vector<Position>(field.soul.begin(), field.soul.end());
      ret.soulsGot = std::vector<bool>(field.soul.size(), false);
      ret.soulCount = field.soulCount;

      ret.estimatedDogDistance = bfs(ret.dogs, [&](Position p){ return canEnter(ret.get(p)); },
				     [&](int d, int x, int y){ ret.farFromDogs.emplace_back(x, y); });
      std::reverse(ret.farFromDogs.begin(), ret.farFromDogs.end());

      ret.estimatedNinjaDistance = bfs(ret.ninjas, [&](Position p){ return canEnter(ret.get(p)); },
				     [&](int d, int x, int y){ ret.farFromNinjas.emplace_back(x, y); });
      std::reverse(ret.farFromNinjas.begin(), ret.farFromNinjas.end());

      ret.estimatedSoulDistance = bfs(ret.dogs, [&](Position p){ return canEnter(ret.get(p)); },
				     [&](int d, int x, int y){ ret.farFromSouls.emplace_back(x, y); });
      std::reverse(ret.farFromSouls.begin(), ret.farFromSouls.end());

      for(int i = 0; i < 2; i++) {
	const auto &n = field.ninja[i];
	ret.field[n.y][n.x] |= i == 0 ? Ninja1 : Ninja2;
      }

      for(int i = 0; i < (int)field.dog.size(); i++) {
	const auto &d = field.dog[i];
	ret.field[d.y][d.x] |= (i + 1) << 4;
      }

      for(int i = 0; i < (int)field.soul.size(); i++) {
	const auto &d = field.soul[i];
	ret.field[d.y][d.x] |= (i + 1) << 12;
      }

      ret.assertField();

      return ret;
    }

    constexpr double dead = -1e9;

    struct Evaluater {
      const AnalyzedField &field;

      std::array<Distances, 2> fromNinja;
      std::array<int, 2> movable;
      std::array<int, 2> maxDistance;
      Distances fromDogs;

      Evaluater(const AnalyzedField &field)
	: field(field)
      {
	auto canGo = [&](Position p){ return canEnter(field.get(p)); };
	fromDogs = bfs(field.dogs, canGo);

	auto canGoWithoutDog = [&](Position p){
	  return canEnter(field.get(p)) && fromDogs[p.y][p.x] >= 1;
	};

	movable[0] = movable[1] = 0;

	fromNinja[0] = bfs({field.ninjas[0]}, canGoWithoutDog, [&](int val, int x, int y) {
	    maxDistance[0] = val;
	    movable[0]++;
	});

	fromNinja[1] = bfs({field.ninjas[1]}, canGoWithoutDog, [&](int val, int x, int y) {
	    maxDistance[1] = val;
	    movable[1]++;
	});
      }

      std::pair<double, double> evaluateSoulDistance() const {
	const int soulCount = field.souls.size();

	const int worst = 30;
	int mins[2] = { worst, worst };

	for(int i = 0; i < soulCount; i++) {
	  if(field.soulsGot[i]) continue;
	  const auto soul = field.souls[i];
	  mins[0] = std::min(mins[0], fromNinja[0][soul.y][soul.x]);
	  mins[1] = std::min(mins[1], fromNinja[1][soul.y][soul.x]);
	}

	return std::make_pair(rSoulDist * (worst - mins[0]) / (double)worst,
			      rSoulDist * (worst - mins[1]) / (double)worst);
      }

      double evaluateSoulGet() const {
	return rSoulGet * std::accumulate(field.soulsGot.begin(), field.soulsGot.end(), 0);
      }

      double evaluateDestroyStone() const {
	return rStoneDestroy * field.destroyedStone;
      }

      double evaluateDogCount() const {
	return field.dogCount * rDogCount;
      }

      std::pair<double, double> evaluateSoulDogDistance() const {
	const int soulCount = field.souls.size();

	const int worst = 30;
	int mins[2] = { worst, worst };

	for(int i = 0; i < soulCount; i++) {
	  if(field.soulsGot[i]) continue;
	  const auto soul = field.souls[i];
	  const int d[2] = {
	    fromNinja[0][soul.y][soul.x],
	    fromNinja[1][soul.y][soul.x]
	  };
	  const int id = d[0] <= d[1] ? 0 : 1;
	  // for(int id = 0; id < 2; id++){
	    if(d[id] == inf) continue; // can not reach
	    if(field.dogs.size() != 0) {
	      const int dogD = fromDogs[soul.y][soul.x];
	      if(d[id] >= dogD * 2) continue; // can not reach before dog reaches
	    }
	    mins[id] = std::min(mins[id], d[id]);
	  // }
	}

	return std::make_pair(rSoulDogDist * (worst - mins[0]) / (double)worst,
			      rSoulDogDist * (worst - mins[1]) / (double)worst);
      }

      std::pair<double, double> evaluateDogDistance() const {
	if(field.dogs.size() == 0) return std::make_pair(0, 0);

	const auto d0 = fromDogs[field.ninjas[0].y][field.ninjas[0].x];
	const auto d1 = fromDogs[field.ninjas[1].y][field.ninjas[1].x];

	const int worst = 30;
	const auto dd0 = rDogDist * std::min(d0, worst) / (double)worst;
	const auto dd1 = rDogDist * std::min(d1, worst) / (double)worst;
	return std::make_pair(dd0, dd1);
      }

      std::pair<double, double> evaluateMovableCount() const {
	const double mx = (height - 2) * (width - 2);
	return std::make_pair(rMovable * movable[0] / mx, rMovable * movable[1] / mx);
      }

      double evaluateNinDist() const {
	const double mx = width - 2 + height - 2;
	const double dx = std::abs(field.ninjas[0].x - field.ninjas[1].x);
	const double dy = std::abs(field.ninjas[0].y - field.ninjas[1].y);
	return rNinDist * ((dx + dy) / mx);
      }

      double evaluateSoulCount() const {
	return (double)(rSoulCount * field.soulCount);
      }

      double getOne(std::pair<double, double> p, int id) const {
	return id == 0 ? p.first : p.second;
      }

      double getAve(std::pair<double, double> p) const {
	return (p.first + p.second) / 2.0;
      }

      double evaluateDogRatio() const {
	int cnts[2] = { 0, 0 };

	for(const auto dog : field.dogs) {
	  if(fromNinja[0][dog.y][dog.x] == fromNinja[1][dog.y][dog.x]) continue;
	  int id = fromNinja[0][dog.y][dog.x] < fromNinja[1][dog.y][dog.x] ? 0 : 1;
	  cnts[id]++;
	}

	if(cnts[0] + cnts[1] == 0) return 0;
	return rDogRatio * std::max(cnts[0], cnts[1]) / (double)(cnts[0] + cnts[1]);
      }

      double evaluate() const {
	if(field.isGameOver()) return dead;

	double ret = 0;
	ret += field.storedScore;
	ret += evaluateSoulCount();
	ret += getAve(evaluateSoulDogDistance());
	ret += getAve(evaluateDogDistance());
	ret += getAve(evaluateSoulDistance());
	ret += getAve(evaluateMovableCount());
	ret += evaluateDogRatio();
	ret += evaluateNinDist();
	ret += evaluateDogCount();
	ret += evaluateSoulGet();
	ret += evaluateDestroyStone();
	return ret;
      }

      double debug(const char *name, double value) const {
	fprintf(stderr, "%s: %.2f\n", name, value);
	return value;
      }

      std::pair<double, double> debug(const char *name, std::pair<double, double> value) const {
	fprintf(stderr, "%s: %.2f (%.2f, %.2f)\n", name, getAve(value), value.first, value.second);
	return value;
      }

      double evaluateVerbose() const {
	if(field.isGameOver()) return dead;

	field.debug();

	double ret = 0;
	ret += debug("stored score", field.storedScore);
	ret += debug("soul count", evaluateSoulCount());
	ret += getAve(debug("soul distance can take", evaluateSoulDogDistance()));
	ret += getAve(debug("dog distance", evaluateDogDistance()));
	ret += getAve(debug("soul distance", evaluateSoulDistance()));
	ret += getAve(debug("movable count", evaluateMovableCount()));
	ret += debug("dog ratio", evaluateDogRatio());
	ret += debug("ninja distance", evaluateNinDist());
	ret += debug("dog count", evaluateDogCount());
	ret += debug("souls get", evaluateSoulGet());
	ret += debug("destroyed stone", evaluateDestroyStone());
	fprintf(stderr, "total: %.2f\n\n", ret);
	return ret;
      }

      double evaluate(int id) const {
	if(field.isGameOver(id)) return dead;

	double ret = field.storedScore;
	ret += evaluateSoulCount();
	ret += getOne(evaluateSoulDogDistance(), id);
	ret += getOne(evaluateDogDistance(), id);
	ret += getOne(evaluateSoulDistance(), id);
	ret += getOne(evaluateMovableCount(), id);
	ret += evaluateDogRatio();
	ret += evaluateNinDist();
	return ret;
      }

      static double evaluate(AnalyzedField &field) {
	return Evaluater(field).evaluate();
      }

      static double evaluateVerbose(AnalyzedField &field) {
	return Evaluater(field).evaluateVerbose();
      }

      static double evaluate(AnalyzedField &field, int id) {
	return Evaluater(field).evaluate(id);
      }
    };

    struct DynamicConfig {
      int useDropStone;
      int useAvatar;

      int weekDropStone;
      int weekAvatar;

      int takeSoulWithAvatar;

      DynamicConfig()
	: useDropStone(100),
	  useAvatar(100),
	  weekDropStone(0),
	  weekAvatar(1),
	  takeSoulWithAvatar(0) {}

      bool mayUseDropStone(const GameInfo &game) const {
	return game.cost[SkillType::DropStoneForEnemy] <= useDropStone;
      }

      bool mayUseAvatar(const GameInfo &game) const {
	return game.cost[SkillType::AvatarForSelf] <= useAvatar;
      }

      bool mayUseAvatarToTakeSoul(const GameInfo &game) const {
	return game.cost[SkillType::AvatarForSelf] <= takeSoulWithAvatar;
      }
    };

    struct DynamicConfigRepo {
      std::vector<std::pair<const char *, DynamicConfig> > configs;
      char current[64];

      DynamicConfigRepo() {
	memset(current, 0, sizeof(current));
      }

      void load(const char *name) {
	FILE *fp = fopen(name, "r");

	if(fp == NULL) {
	  fprintf(stderr, "Cannot load config repo\n");
	  fflush(stderr);
	  return;
	}

	char buff[64];
	int a, b, c, d, e;
	while(fscanf(fp, "%s %d %d %d %d %d", buff, &a, &b, &c, &d, &e) != EOF) {
	  DynamicConfig config;
	  config.useDropStone = a;
	  config.useAvatar = b;
	  config.weekDropStone = c;
	  config.weekAvatar = d;
	  config.takeSoulWithAvatar = e;
	  configs.emplace_back(strdup(buff), config);
	}

	fclose(fp);
      }

      DynamicConfig defaultConfig() const {
	return DynamicConfig();
      }

      DynamicConfig readFromNameFile(const char *path) {
	FILE *fp = fopen(path, "r");

	if(fp == NULL) {
	  fprintf(stderr, "Cannot read name file\n");
	  fflush(stderr);
	  return defaultConfig();
	}

	char buff[64] = {0};
	fscanf(fp, "%s", buff);
	fclose(fp);


	if(strcmp(current, buff) != 0) {
	  strcpy(current, buff);
	  fprintf(stderr, "VS %s\n", current);
	  fflush(stderr);
	}

	for(const auto &p : configs) {
	  if(strcmp(buff, p.first) == 0)
	    return p.second;
	}

	return defaultConfig();
      }
    };

    struct SimpleTactics {
      struct Config {
	int forceSkill;
	int coreCount;

	Config() : forceSkill(0), coreCount(2) {
	}
      };

      AnalyzedField &field;
      Config &config;
      const DynamicConfig &dyn;

      SimpleTactics(AnalyzedField &field, Config &config, const DynamicConfig &dyn)
	: field(field), config(config), dyn(dyn) {
      }

      typedef std::pair<double, std::vector<Output> > data;

      struct cmp {
	bool operator () (const data &lhs, const data &rhs) const {
	  return lhs.first > rhs.first;
	}
      };

      static bool isSafeLast(AnalyzedField &field, const std::vector<Output> &out, const DynamicConfig &dyn) {
	if(!dyn.mayUseAvatar(field.game) && !dyn.mayUseDropStone(field.game)) {
	  return true;
	}

	bool ret = true;
	std::vector<AnalyzedField::Undo> undos;

	for(int i = 0; i < (int)out.size() - 1; i++){
	  undos.push_back(field.simulateTurn(out[i]));
	}

	std::vector<Position> cand;
	std::vector<Position> lasts;
	Output last = out.back();

	if(dyn.mayUseDropStone(field.game)) {
	  for(int id = 0; id < 2; id++) {
	    std::vector<AnalyzedField::Undo> undos2;

	    for(int i = 0; i < (int)last.move[id].size(); i++) {
	      const auto dir = last.move[id][i];
	      auto pos = field.ninjas[id];
	      if(hasStone(field.get(pos.move(dir)))) {
		cand.push_back(pos.move(dir).move(dir));
	      } else {
		cand.push_back(pos.move(dir));
	      }
	      if(i != (int)last.move[id].size() - 1) {
		if(field.canMoveNinja(id, std::vector<Dir>(1, dir)))
		  undos2.push_back(field.moveNinja(id, dir));
	      } else {
		lasts.push_back(pos.move(dir));
	      }
	    }

	    field.applyUndos(undos2);
	  }

	  for(auto pos : cand) {
	    if(hasStone(field.get(pos))) continue;

	    std::vector<AnalyzedField::Undo> undos2;
	    undos2.push_back(field.addStone(pos.x, pos.y));
	    undos2.push_back(field.simulateTurn(last));

	    if(field.isGameOver()) ret = false;
	    field.applyUndos(undos2);
	    if(!ret) break;
	  }
	}

	if(!ret) {
	  field.applyUndos(undos);
	  return false;
	}

	if(dyn.mayUseAvatar(field.game) && last.skill.size() == 1 && last.skill[0].type == SkillType::AvatarForSelf) {
	  lasts.push_back(field.ninjas[0]);
	  lasts.push_back(field.ninjas[1]);
	  for(auto &pos : lasts) {
	    last.skill.push_back(avatarForSelf(pos.x, pos.y));
	    auto undo = field.simulateTurn(last);
	    if(field.isGameOver()) ret = false;
	    last.skill.pop_back();
	    undo(field);
	    if(!ret) break;
	  }
	}
	field.applyUndos(undos);
	return ret;
      }

      std::pair<double, Output> run() {
	int lim = 5;
	if(field.game.restMS < 2 * 60 * 1000) lim = 3;
	int width = config.coreCount * 3;
	if(field.game.restMS < 2 * 60 * 1000) width = config.coreCount * 2;

	for(int depth = lim; depth >= 1; depth--){
	  const std::vector<data> src(1, std::make_pair(0.0, std::vector<Output>()));
	  const auto search = beamSearch(0, depth, width, src);
	  if(!search.empty() && search[0].first != dead && !search[0].second.empty()) {
	    if(config.forceSkill > 0) config.forceSkill--;

#ifdef DEBUG
	    std::vector<AnalyzedField::Undo> undos;
	    for(const auto &output : search[0].second) {
	      undos.push_back(field.simulateTurn(output));
	    }
	    Evaluater::evaluateVerbose(field);
	    field.applyUndos(undos);
#endif

	    const auto outs = search[0].second;
	    if(outs.size() >= 3) {
	      if(outs[0].skill.size() > 0 && outs[0].skill[0].type == SkillType::AvatarForSelf &&
		 outs[2].skill.size() > 0 && outs[2].skill[0].type == SkillType::AvatarForSelf) {
		auto ninjas = field.ninjas;
		auto starts = ninjas;

		for(int i = 0; i < 2; i++) {
		  for(int j = 0; j < 2; j++) {
		    for(int k = 0; k < (int)outs[i].move[j].size(); k++) {
		      ninjas[j] = ninjas[j].move(outs[i].move[j][k]);
		    }
		  }
		}

		if(ninjas[0] == starts[0] && ninjas[1] == starts[1]) {
		  config.forceSkill = 4;
		}
	      }
	    }

	    return std::make_pair(search[0].first, search[0].second[0]);
	  }
	}

	return std::make_pair(dead, Output());
      }

      std::vector<data> beamSearch(int cur, int depth, int width, const std::vector<data> &src) {
	const int coreCount = config.coreCount;

	if(cur == depth) {
	  return src;
	}

	std::vector<std::vector<data> > buff(coreCount);
	{
	  std::vector<std::thread> threads;
	  for(int c = 0; c < coreCount; c++) {
	    auto f = [&buff, &src, c, cur, coreCount, this]() {
	      Config config = this->config;
	      AnalyzedField field = this->field;
	      for(int j = c; j < (int)src.size(); j += coreCount) {
		const auto &d = src[j];
		std::vector<AnalyzedField::Undo> undos;

		for(const auto &tactics : d.second) {
		  undos.push_back(field.simulateTurn(tactics));
		}

		auto tmp = d.second;

		for(const auto &s : simulateAll(field, cur, config)) {
		  const double score = s.first;
		  const auto &output = s.second[0];

		  tmp.push_back(output);
		  buff[c].push_back(std::make_pair(score, tmp));
		  tmp.pop_back();
		}

		field.applyUndos(undos);
	      }
	    };

	    threads.push_back(std::thread(f));
	  }

	  for(auto &t : threads) {
	    t.join();
	  }
	}

	int sz = 0;
	for(int i = 0; i < coreCount; i++) {
	  sz += (int)buff[i].size();
	}

	std::vector<data> next; next.reserve(sz);
	for(int i = 0; i < coreCount; i++) {
	  for(const auto &d : buff[i])
	    next.push_back(d);
	}

	std::sort(next.begin(), next.end(), cmp());

	std::vector<data> filtered; filtered.reserve(width);
	{
	  const int lim = std::min<int>(next.size(), cur == 0 ? next.size() : width * 2);
	  std::vector<int> ok(lim);
	  std::vector<std::thread> threads;
	  for(int c = 0; c < coreCount; c++) {
	    auto f = [&ok, &next, c, lim, coreCount, this]() {
	      AnalyzedField field = this->field;
	      for(int j = c; j < lim; j += coreCount) {
		ok[j] = isSafeLast(field, next[j].second, this->dyn);
	      }
	    };

	    threads.push_back(std::thread(f));
	  }

	  for(auto &t : threads) {
	    t.join();
	  }

	  for(int i = 0; i < lim && (int)filtered.size() < width; i++) {
	    if(ok[i]) filtered.push_back(next[i]);
	  }

	  if(filtered.size() == 0 && depth <= 2) {
	    filtered = next;
	    if((int)filtered.size() > width)
	      filtered.erase(filtered.begin() + width, filtered.end());
	  }
	}

	return beamSearch(cur + 1, depth, width, filtered);
      }

      static std::vector<data> simulateAll(AnalyzedField &field, int level, Config &config) {
	std::vector<data> ret;
	Output dummy;
	dfs(dummy, ret, field, config, level);
	return ret;
      }

      static void dfs(Output &base, std::vector<data> &ret, AnalyzedField &field, Config &config, int level) {
	if(field.canSpell(SkillType::FastMove) && level == 0){
	  base.skill.push_back(fastMove());
	  auto undo = field.simulateSkill(base.skill[0]);

	  if(field.game.turn < 15 && field.game.cost[SkillType::FastMove] <= 1) {
	    field.storedScore += rFastMove;
	  }

	  dfs(base, 0, ret, field, config);

	  if(field.game.turn < 15 && field.game.cost[SkillType::FastMove] <= 1) {
	    field.storedScore -= rFastMove;
	  }

	  base.skill.pop_back();
	  undo(field);
	}

	if(field.canSpell(SkillType::TransportDogs)) {
	  for(int id = 0; id < 2; id++) {
	    const auto ninja = field.ninjas[id];
	    int cnt = 0;
	    for(int y = -1; y <= 1; y++) {
	      for(int x = -1; x <= 1; x++) {
		const Position pos(ninja.x + x, ninja.y + y);
		if(hasNinDog(field.get(pos))) cnt++;
	      }
	    }
	    if(cnt < 4) continue;

	    base.skill.push_back(transportDogs(id));
	    auto undo = field.simulateSkill(base.skill[0]);
	    dfs(base, 0, ret, field, config);
	    base.skill.pop_back();
	    undo(field);
	  }
	}

	if(field.canSpell(SkillType::DestroyStoneForSelf)) {
	  for(int id = 0; id < 2; id++) {
	    const auto ninja = field.ninjas[id];
	    for(int i = 0; i < size2; i++) {
	      const int x = ninja.x + dx2[i];
	      const int y = ninja.y + dy2[i];
	      const Position pos(x, y);

	      if(!pos.isValid()) continue;

	      if(hasStone(field.get(pos))) {
		base.skill.push_back(destroyStoneForSelf(pos.x, pos.y));
		auto undo = field.simulateSkill(base.skill[0]);
		dfs(base, 0, ret, field, config);
		base.skill.pop_back();
		undo(field);
	      }
	    }
	  }
	}

	if(field.canSpell(SkillType::AvatarForSelf) && field.dogs.size() > 0) {
	  const int lim = 2;
	  auto trySpell = [&](Position pos) {
	    field.assertField();
	    if(canEnter(field.get(pos))) {
	      base.skill.push_back(avatarForSelf(pos.x, pos.y));
	      auto undo = field.simulateSkill(base.skill[0]);
	      dfs(base, 0, ret, field, config);
	      undo(field);
	      base.skill.pop_back();
	    }
	  };

	  for(int i = 0; i < lim; i++) {
	    if(i < (int)field.farFromDogs.size()) {
	      const auto pos = field.farFromDogs[i];
	      trySpell(pos);
	    }
	    if(i < (int)field.farFromSouls.size()) {
	      const auto pos = field.farFromSouls[i];
	      trySpell(pos);
	    }
	    if(i < (int)field.farFromNinjas.size()) {
	      const auto pos = field.farFromNinjas[i];
	      trySpell(pos);
	    }
	  }

	  for(const auto pos : field.ninjas) {
	    trySpell(pos);
	    for(int i = 0; i < 4; i++) {
	      trySpell(pos.move((Dir)i));
	    }
	  }
	}

	if(config.forceSkill == 0 || ret.size() == 0) {
	  dfs(base, 0, ret, field, config);
	}
      }

      static void dfs(Output &base, int id, std::vector<data> &ret, AnalyzedField &field, Config &config) {
	if(id == 2){
	  std::vector<Position> avatar;
	  if(!base.skill.empty() && base.skill[0].type == SkillType::AvatarForSelf) {
	    avatar.emplace_back(base.skill[0].args[1], base.skill[0].args[0]);
	  } else {
	    for(int id = 0; id < 2; id++) {
	      const auto ninja = field.ninjas[id];
	      if(hasNinDog(field.get(ninja))) {
		// Dead
		return;
	      }
	    }
	  }
	  auto undo = field.moveDog1Step(avatar);
	  const double score = Evaluater::evaluate(field);
	  if(score != dead) {
	    ret.push_back(std::make_pair(score, std::vector<Output>(1, base)));
	  }
	  undo(field);
	  return;
	} else {
	  dfs(base, id + 1, ret, field, config);

	  int lim = 2;
	  if(base.skill.size() != 0 && base.skill[0].type == SkillType::FastMove)
	    lim = 3;

	  if((int)base.move[id].size() == lim) return;

	  if(lim == 3 && id == 0 && base.move[id].size() == 0) {
	    std::vector<std::thread> threads;
	    std::vector<data> vec[4];

	    for(int d = 0; d < 4; d++) {
	      const Dir dir = (Dir)d;
	      if(field.canMoveNinja(field.ninjas[id], dir)) {
		auto f = [id, &base, dir, &field, &config, &vec](){
		  Output base_ = base;
		  AnalyzedField field_ = field;
		  Config config_ = config;
		  base_.move[id].push_back(dir);
		  field_.moveNinja(id, dir);
		  dfs(base_, id, vec[dir], field_, config_);
		  base_.move[id].pop_back();
		};
		threads.push_back(std::thread(f));
		field.assertField();
	      }
	    }

	    for(auto &t : threads) t.join();

	    for(int i = 0; i < 4; i++)
	      for(auto &d : vec[i])
		ret.push_back(d);

	    return;
	  }

	  if(base.move[id].size() == 2) {
	    for(int i = 0; i < 2; i++) {
	      const Dir dir = (Dir)base.move[id][i];
	      if(field.canMoveNinja(field.ninjas[id], dir)) {
		field.assertField();
		base.move[id].push_back(dir);
		std::vector<AnalyzedField::Undo> undos;
		undos.push_back(field.moveNinja(id, dir));
		dfs(base, id, ret, field, config);
		field.applyUndos(undos);
		base.move[id].pop_back();
	      }
	    }
	  } else {
	    for(int d = 0; d < 4; d++) {
	      const Dir dir = (Dir)d;
	      if(field.canMoveNinja(field.ninjas[id], dir)) {
		field.assertField();
		base.move[id].push_back(dir);
		std::vector<AnalyzedField::Undo> undos;
		undos.push_back(field.moveNinja(id, dir));
		dfs(base, id, ret, field, config);
		field.applyUndos(undos);
		base.move[id].pop_back();
	      }
	    }
	  }
	}
      }
    };
  } }

std::vector<CodeVS::SkillInfo> hiretsu(const char *in) {
#if __MINGW32__
  const char *buffer = "codevs/tmp";
#else
  char buffer[L_tmpnam];
  tmpnam(buffer);
#endif
  FILE *fp = fopen(buffer, "w");
  fprintf(fp, "%s", in);
  fclose(fp);
  const char *inFile = buffer;

#if __MINGW32__
  const char *command = "cd codevs && java Hiretsu < tmp";
  FILE *result = popen(command, "r");
#else
  std::string command = std::string("cd codevs && java Hiretsu < ") + inFile;
  FILE *result = popen(command.c_str(), "r");
#endif

  std::vector<CodeVS::SkillInfo> ret;

  int num;
  fscanf(result, "%d", &num);

  if(num == 3) {
    int type;
    fscanf(result, "%d", &type);
    CodeVS::SkillInfo info((CodeVS::SkillType)type);
    for(int i = 0; i < CodeVS::skillArgCounts[type]; i++) {
      int arg;
      fscanf(result, "%d", &arg);
      info.args.push_back(arg);
    }
    ret.push_back(info);
  }

  pclose(result);
  remove(inFile);

  return ret;
}

int main(int argc, char **argv) {
  if(argc != 4) {
    fprintf(stderr, "must supply 3 args\n");
    fflush(stderr);
    return 0;
  }

  const auto AIName = "MilkyHolmesLove";
  const int core = CodeVS::AI::coreCount;
  printf("%s_%d\n", AIName, core);
  fflush(stdout);

  CodeVS::AI::readConfig(argv[1]);
  auto in = CodeVS::Input();
  auto repo = CodeVS::AI::DynamicConfigRepo(); repo.load(argv[2]);
  auto config = CodeVS::AI::SimpleTactics::Config();

  while(true) {
    try {
      const auto game = in.read();
      const auto &field = game.myField;
      const auto dyn = repo.readFromNameFile(argv[3]);

      std::vector<CodeVS::SkillInfo> skills;
      std::vector<std::thread> threads;

      if(dyn.weekDropStone || dyn.weekAvatar) {
	auto analyzed = CodeVS::AI::analyze(game.enemyField, game);
	auto fromDog = CodeVS::AI::bfs(analyzed.dogs, [&](CodeVS::AI::Position pos){
	    return CodeVS::AI::canEnter(analyzed.get(pos));
	  });
	bool toSearch = false;

	for(int i = 0; i < 2; i++) {
	  const auto ninja = analyzed.ninjas[i];
	  if(fromDog[ninja.y][ninja.x] <= 2)
	    toSearch = true;
	}

	if(toSearch) {
	  threads.emplace_back([&skills, &in]() {
	      skills = hiretsu(in.buff);
	   });
	}
      }

      auto analyzed = CodeVS::AI::analyze(field, game);
      config.coreCount = core - threads.size();
      auto output = CodeVS::AI::SimpleTactics(analyzed, config, dyn).run().second;

      for(auto &thread : threads)
	thread.join();

      if(skills.size() && output.skill.size() == 0) {
	const int type = skills[0].type;
	if((type == CodeVS::SkillType::DropStoneForEnemy && dyn.weekDropStone) ||
	   (type == CodeVS::SkillType::AvatarForEnemy && dyn.weekAvatar))
	  output.skill.push_back(skills[0]);
      }

      output.write();
    } catch(int e) {
    }
  }

  return 0;
}
